#define _GNU_SOURCE		/* syscall() is not POSIX, O_DIRECT */

#include <fcntl.h>
#include <inttypes.h>
#include <linux/aio_abi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

#include "generators.h"

/* thin syscall wrappers */

int io_setup(unsigned nr, aio_context_t *ctxp)
{
	return syscall(__NR_io_setup, nr, ctxp);
}

int io_destroy(aio_context_t ctx)
{
	return syscall(__NR_io_destroy, ctx);
}

int io_submit(aio_context_t ctx, long nr,  struct iocb **iocbpp)
{
	return syscall(__NR_io_submit, ctx, nr, iocbpp);
}

int io_getevents(aio_context_t ctx, long min_nr, long max_nr,
		struct io_event *events, struct timespec *timeout)
{
	return syscall(__NR_io_getevents, ctx, min_nr, max_nr, events, timeout);
}

#define	PAGE_SIZE			4096
#define	PROGRESS_INTERVAL	1.0


static void
actually_read_data(struct iocb *queue, int queue_size)
{
	int		i;
	char	buffer[PAGE_SIZE];

	for (i = 0; i < queue_size; i++)
	{
		/* if we fail we can't really do much about it except for logging it */
		if (pread(queue[i].aio_fildes, buffer, queue[i].aio_nbytes, queue[i].aio_offset) == -1)
			fprintf(stderr, "pread failure (fd=%d offset=%lld bytes=%lld errno=%d %s)\n",
					queue[i].aio_fildes, queue[i].aio_offset, queue[i].aio_nbytes, errno, strerror(errno));
	}
}


int main(int argc, const char *argv[])
{
	int ret;
	int retval;
	int fd;
	int i;
	struct timeval	tv1;
	double	elapsed, next_elapsed;
	mode_t	mode;

	aio_context_t	ctx;
	struct iocb	   *cb;
	struct iocb	  **cbs;
	char		  **buffers;
	struct io_event *events;
	long int		nrequests = 0;
	long int		nrequests_progress = 0;

	page_generator_cb next_page;
	struct stat 	s;
	long int		num_pages;

	/* page generator state */
	void			   *gstate;
	sequential_state_t	sstate;
	random_state_t		rstate;

	/* command-line parameters */
	const char *test_mode;
	const char *workload;
	const char *iomode;
	const char *filename;
	int			max_duration;
	int			num_threads;
	int			queue_size;
	double		start_prob;
	double		continue_prob;

	/* warmup */
	int			warmup;
	double		warmup_prev_speed = 0.0;
	double		warmup_prev_elapsed = 0.0;
	long int	warmup_prev_nrequests = 0;

	/*
	 * read command-line parameters
	 *
	 * aio-test [WARMUP|TEST] FILENAME [DIRECT|BUFFERED] WORKLOAD DURATION QUEUE_SIZE [START_PROB] [CONTINUE_PROB]
	 *
	 * XXX No error checking here. Meh ...
	 */
	if ((argc < 7) || (argc > 9))
	{
		fprintf(stderr, "invalid number (%d) of command-line parameters\n", argc);
		exit(1);
	}

	test_mode = (char *) argv[1];
	filename = (char *) argv[2];
	iomode = (char *) argv[3];
	workload = (char *) argv[4];
	max_duration = strtol(argv[5], NULL, 10);
	queue_size = strtol(argv[6], NULL, 10);

	/* perfectly sequential by default */
	start_prob = (argc > 7) ? strtod(argv[7], NULL) : 1.0;
	continue_prob = (argc > 8) ? strtod(argv[8], NULL) : 1.0;

	/* print input parameters */
	fprintf(stderr, "type: aio\n");
	fprintf(stderr, "filename: %s\n", filename);
	fprintf(stderr, "I/O mode: %s\n", iomode);
	fprintf(stderr, "workload: %s\n", workload);
	fprintf(stderr, "duration: %d\n", max_duration);
	fprintf(stderr, "queue size: %d\n", queue_size);
	fprintf(stderr, "test mode: %s\n", test_mode);

	/* FIXME check valid queue_size */

	cb = (struct iocb *) malloc(sizeof(struct iocb) * queue_size);
	cbs = (struct iocb **) malloc(sizeof(struct iocb *) * queue_size);
	buffers = (char **) malloc(sizeof(char *) * queue_size);
	events = (struct io_event *) malloc(sizeof(struct io_event) * queue_size);

	for (i = 0; i < queue_size; i++)
	{
		if (posix_memalign((void **) &buffers[i], PAGE_SIZE, PAGE_SIZE) != 0)
		{
			fprintf(stderr, "posix_memalign: failed to allocate buffer\n");
			exit(10);
		}
	}

	/* open the file */
	if (strcmp(iomode, "BUFFERED") == 0)
		mode = O_RDONLY;
	else if (strcmp(iomode, "DIRECT") == 0)
		mode = (O_RDONLY | O_DIRECT);
	else
	{
		fprintf(stderr, "invalid I/O mode specified: %s\n", iomode);
		exit(2);
	}

	fd = open(filename, mode);
	if (fd < 0)
	{
		fprintf(stderr, "can't open file '%s' (mode: %d)\n", filename, mode);
		exit(3);
	}

	/* determine file size etc. */
	if (fstat(fd, &s) != 0)
	{
		fprintf(stderr, "can't stat file '%s'\n", filename);
		exit(4);
	}

	num_pages = (s.st_size / PAGE_SIZE);

	fprintf(stderr, "file size: %ld\n", s.st_size);
	fprintf(stderr, "file pages: %ld\n", num_pages);

	/* initialize the generator matching the workload type */
	if (strcmp(workload, "RANDOM") == 0)
	{
		if (argc != 7)
		{
			fprintf(stderr, "incorrect number of arguments (%d) for random workload\n", argc);
			exit(5);
		}

		next_page = random_next;
		random_init(&rstate, num_pages);
		gstate = (void *) &rstate;
	}
	else if (strcmp(workload, "SEQUENTIAL") == 0)
	{
		next_page = sequential_next;
		sequential_init(&sstate, num_pages, start_prob, continue_prob);
		gstate = (void *) &sstate;

		fprintf(stderr, "start probability: %.3f\n", start_prob);
		fprintf(stderr, "continue probability: %.3f\n", continue_prob);
	}
	else
	{
		fprintf(stderr, "unknown workload type\n");
		exit(6);
	}

	if (strcmp(test_mode, "WARMUP") == 0)
	{
		warmup = 1;
		retval = 1;
	}
	else if (strcmp(test_mode, "TEST") == 0)
	{
		warmup = 0;
		retval = 0;
	}
	else
	{
		fprintf(stderr, "unknown test mode\n");
		exit(7);
	}

	ctx = 0;

	ret = io_setup(queue_size, &ctx);
	if (ret < 0) {
		perror("io_setup error");
		return -1;
	}

	gettimeofday(&tv1, NULL);
	elapsed = 0;
	next_elapsed = PROGRESS_INTERVAL;

	while (1)
	{
		int npending;

		/* setup I/O control block */
		memset(cb, 0, sizeof(struct iocb) * queue_size);

		for (i = 0; i < queue_size; i++)
		{
			cb[i].aio_fildes = fd;
			cb[i].aio_lio_opcode = IOCB_CMD_PREAD;

			/* command-specific options */
			cb[i].aio_buf = (uint64_t) buffers[i];
			cb[i].aio_offset = next_page(gstate) * PAGE_SIZE;
			cb[i].aio_nbytes = PAGE_SIZE;

			cbs[i] = &cb[i];
		}

		nrequests += queue_size;

		ret = io_submit(ctx, queue_size, cbs);
		if (ret != queue_size)
		{
			if (ret < 0)
				perror("io_submit error");
			else
				fprintf(stderr, "could not sumbit IOs");
			return  -1;
		}

		/* get replies for all pending requests */
		npending = queue_size;
		while (npending > 0)
		{
			ret = io_getevents(ctx, npending, queue_size, events, NULL);

			for (i = 0; i < ret; i++)
			{
				/* XXX: can we get multiple responses for a single request? */
				if (events[i].res == PAGE_SIZE)
					npending--;
				else
					fprintf(stderr, "unexpected res = %lld (res2 = %lld)\n", events[i].res, events[i].res2);
			}
		}

		/* actually read the data, just like we'd do in BHS */
		if (strcmp(iomode, "BUFFERED") == 0)
			actually_read_data(cb, queue_size);

		/* print the progress info every 5 seconds or so */
		if (nrequests >= nrequests_progress)
		{
			double		speed;
			long int	nrequests_next;
			struct timeval	tv2;

			gettimeofday(&tv2, NULL);
			elapsed = (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec) / 1000000.0;

			/* requests per second */
			speed = (nrequests / elapsed);

			/* if we haven't reached the next progress threshold, just update
			 * the number of requests and continue */
			if (elapsed < next_elapsed)
			{
				nrequests_next = next_elapsed * speed;

				/*
				 * don't increment too fast - double at most
				 *
				 * The initial batches of requests may be unexpectedly high (due
				 * to caching or something like that), resulting in far too high
				 * threshold. Battle that by limiting the growth to doubling.
				 */
				if (nrequests_next > nrequests_progress * 2)
					nrequests_next = nrequests_progress * 2;

				nrequests_progress = nrequests_next;

				continue;
			}

			fprintf(stderr, "processed %ld requests %.2f r/s elapsed %.2f\n", nrequests, nrequests/elapsed, elapsed);

			/*
			 * compute the next point in time to print progress report, and
			 * the estimated number of records at that point
			 */
			next_elapsed = PROGRESS_INTERVAL * (floor(elapsed / PROGRESS_INTERVAL) + 1);
			nrequests_next = next_elapsed * speed;

			/*
			 * don't increment too fast - double at most
			 *
			 * The initial batches of requests may be unexpectedly high (due
			 * to caching or something like that), resulting in far too high
			 * threshold. Battle that by limiting the growth to doubling.
			 */
			if (nrequests_next > nrequests_progress * 2)
				nrequests_next = nrequests_progress * 2;

			nrequests_progress = nrequests_next;

			/* now decide what to do if we're in warmup mode */
			if (warmup)
			{
				/* first compute the speed in this interval (delta elapsed should be about PROGRESS_INTERVAL) */
				double		delta_elapsed = (elapsed - warmup_prev_elapsed);
				long int	delta_nrequests = (nrequests - warmup_prev_nrequests);
				double		delta_speed = delta_nrequests / delta_elapsed;

				fprintf(stderr, "elapsed %.2f : warmup interval speed %.2f previous %.2f\n", elapsed, delta_speed, warmup_prev_speed);

				/* if we got within 5% of the previous interval, warmup is done */
				if ((warmup_prev_nrequests > 0) && (delta_speed < warmup_prev_speed * 1.05))
				{
					fprintf(stderr, "within 5%% of the preceding interval, warmup terminating\n");
					retval = 0;
					break;
				}

				/* otherwise just remember data from this interval */
				warmup_prev_nrequests = nrequests;
				warmup_prev_speed = delta_speed;
				warmup_prev_elapsed = elapsed;
			}
			else if (elapsed >= max_duration)
			{
				/* test (non-warmup) mode ends when the time expires */
				break;
			}
		}
	}

	/* final message (print it always) */
	fprintf(stderr, "TOTAL: %ld requests %.2f r/s elapsed %.2f\n", nrequests, nrequests / elapsed, elapsed);

	ret = io_destroy(ctx);
	if (ret < 0) {
		perror("io_destroy error");
		return -1;
	}

	return retval;
}
