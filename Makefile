all: pthread-test aio-test paio-test pthread-fadvise-test fadvise-test

clean:
	rm -f *.o pthread-test aio-test paio-test pthread-fadvise-test fadvise-test

pthread-test:
	gcc pthread-test.c generators.c -O2 -g -lpthread -o pthread-test

pthread-fadvise-test:
	gcc pthread-fadvise-test.c generators.c -O2 -g -lpthread -o pthread-fadvise-test

fadvise-test:
	gcc fadvise-test.c generators.c -O2 -g -lpthread -o fadvise-test

aio-test:
	gcc aio-test.c generators.c -O2 -g -lrt -o aio-test

paio-test:
	gcc paio-test.c generators.c -O2 -g -lrt -o paio-test
