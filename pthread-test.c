#define _GNU_SOURCE		/* O_DIRECT */

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "generators.h"

#define	PAGE_SIZE			4096
#define	PROGRESS_INTERVAL	1.0
#define	MAX_CHUNK_SIZE		32

typedef struct io_request_t
{
	int		fd;
	size_t	offset;
	size_t	nbytes;
} io_request_t;

typedef struct io_queue_t
{
	/* held while manipulating the queue */
	pthread_mutex_t		lock;

	/* triggered after adding a request to the queue */
	pthread_cond_t		io_submitted;

	/* triggered by I/O threads after completing an I/O */
	pthread_cond_t		io_completed;

	/* next request (not yet taken by any thread) */
	int		next;

	/* number of currently scheduled I/O requests (first unused index) */
	int		nrequests;

	/* number of pending (not completed) requests */
	int		npending;

	/* how many requests to grab per round */
	int		chunk_size;

	/* number of threads processing queued requests */
	int		nthreads;

	/* array of requests */
	io_request_t   *requests;

} io_queue_t;

/* single I/O queue */
static	io_queue_t	queue;

/* I/O threads */
static	pthread_t  *threads;

static void
init_queue(io_queue_t *queue, int nrequests, int nthreads)
{
	/* initialize the queue spinlock and conditional variable */
	pthread_mutex_init(&queue->lock, NULL);
	pthread_cond_init(&queue->io_submitted, NULL);
	pthread_cond_init(&queue->io_completed, NULL);

	queue->next = 0;
	queue->nrequests = 0;
	queue->npending = 0;
	queue->nthreads = nthreads;
	queue->chunk_size = 0;

	queue->requests = (io_request_t *) malloc(sizeof(io_request_t) * nrequests);
}


static void
actually_read_data(io_queue_t *queue)
{
	int		i;
	char	buffer[PAGE_SIZE];

	for (i = 0; i < queue->nrequests; i++)
	{
		/* if we fail we can't really do much about it except for logging it */
		if (pread(queue->requests[i].fd, buffer, queue->requests[i].nbytes, queue->requests[i].offset) == -1)
			fprintf(stderr, "pread failure (fd=%d offset=%ld bytes=%ld errno=%d %s)\n",
					queue->requests[i].fd, queue->requests[i].offset, queue->requests[i].nbytes, errno, strerror(errno));
	}
}

/*
 * XXX Maybe this should grab multiple requests from the queue, not one.
 * That should reduce the lock contention, if it happens to be an issue.
 *
 * XXX This could also use posix_fadvise(POSIX_FADV_WILLNEED) instead of
 * pread pretty easily. Not sure it'd be better or not.
 */
static void *
prefetch_worker(void *data)
{
	int				i;
	io_queue_t	   *queue = (io_queue_t *) data;

	int 			nrequests;
	io_request_t	requests[MAX_CHUNK_SIZE];
	char		   *buffer;

	/* needed for direct I/O */
	if (posix_memalign((void **) &buffer, PAGE_SIZE, PAGE_SIZE) != 0)
	{
		fprintf(stderr, "posix_memalign: failed to allocate buffer\n");
		exit(10);
	}

	pthread_mutex_lock(&queue->lock);

	/* if there are no waiting I/O requests, wait */
	while (1)
	{
		while (queue->next == queue->nrequests)
			pthread_cond_wait(&queue->io_submitted, &queue->lock);

		/* someone else grabbed the request, go back to sleep */
		if (queue->next == queue->nrequests)
			continue;

		/* wooohooo, there are requests, grab a couple of them ... */

		/* if we're the first for this round, compute chunk_size */
		if (queue->next == 0)
		{
			queue->chunk_size = queue->nrequests / queue->nthreads;

			/* make sure the chunk size is reasonable */
			if (queue->chunk_size < 1)
				queue->chunk_size = 1;
			else if (queue->chunk_size > MAX_CHUNK_SIZE)
				queue->chunk_size = MAX_CHUNK_SIZE;
		}

		nrequests = queue->chunk_size;
		if (nrequests > (queue->nrequests - queue->next))
			nrequests = queue->nrequests - queue->next;

		memcpy(requests, &queue->requests[queue->next], sizeof(io_request_t) * nrequests);

		queue->next += nrequests;

		/* stop blocking the queue, start working on it */
		pthread_mutex_unlock(&queue->lock);

		for (i = 0; i < nrequests; i++)
		{
			/* if we fail we can't really do much about it except for logging it */
			if (pread(requests[i].fd, buffer, requests[i].nbytes, requests[i].offset) == -1)
				fprintf(stderr, "pread failure\n");
		}

		/* lock again, update pending, trigger "complete" CV if this was
		 * the last one */
		pthread_mutex_lock(&queue->lock);
		queue->npending -= nrequests;
		if (queue->npending == 0)
			pthread_cond_broadcast(&queue->io_completed);

		/* and to the next I/O request (keep holding the lock so that
		 * we can inspect the condition variable) */
	}

	return NULL;
}

int main(int argc, const char *argv[])
{
	int				i;
	int				fd;
	int				ret;
	mode_t			mode;
	struct timeval	tv1;
	double			elapsed;
	double			next_elapsed;
	page_generator_cb next_page = sequential_next;
	struct stat 	s;
	long int		nrequests = 0;
	long int 		nrequests_progress = 1000;
	long int		num_pages;
	int				retval;

	/* page generator state */
	void			   *gstate;
	sequential_state_t	sstate;
	random_state_t		rstate;

	/* command-line parameters */
	const char *test_mode;
	const char *workload;
	const char *iomode;
	const char *filename;
	int			max_duration;
	int			num_threads;
	int			queue_size;
	double		start_prob;
	double		continue_prob;

	/* warmup */
	int			warmup;
	double		warmup_prev_speed = 0.0;
	double		warmup_prev_elapsed = 0.0;
	long int	warmup_prev_nrequests = 0;

	/*
	 * read command-line parameters
	 *
	 * pthread-test [WARMUP|TEST] FILENAME [DIRECT|BUFFERED] WORKLOAD DURATION QUEUE_SIZE THREADS [START_PROB] [CONTINUE_PROB]
	 *
	 * XXX No error checking here. Meh ...
	 */
	if ((argc < 8) || (argc > 10))
	{
		fprintf(stderr, "invalid number (%d) of command-line parameters\n", argc);
		exit(1);
	}

	test_mode = (char *) argv[1];
	filename = (char *) argv[2];
	iomode = (char *) argv[3];
	workload = (char *) argv[4];
	max_duration = strtol(argv[5], NULL, 10);
	queue_size = strtol(argv[6], NULL, 10);
	num_threads = strtol(argv[7], NULL, 10);

	/* perfectly sequential by default */
	start_prob = (argc > 8) ? strtod(argv[8], NULL) : 1.0;
	continue_prob = (argc > 9) ? strtod(argv[9], NULL) : 1.0;

	/* print input parameters */
	fprintf(stderr, "type: pthread aio\n");
	fprintf(stderr, "filename: %s\n", filename);
	fprintf(stderr, "I/O mode: %s\n", iomode);
	fprintf(stderr, "workload: %s\n", workload);
	fprintf(stderr, "duration: %d\n", max_duration);
	fprintf(stderr, "threads: %d\n", num_threads);
	fprintf(stderr, "queue size: %d\n", queue_size);
	fprintf(stderr, "test mode: %s\n", test_mode);

	/* open the file */
	if (strcmp(iomode, "BUFFERED") == 0)
		mode = O_RDONLY;
	else if (strcmp(iomode, "DIRECT") == 0)
		mode = (O_RDONLY | O_DIRECT);
	else
	{
		fprintf(stderr, "invalid I/O mode specified: %s\n", iomode);
		exit(2);
	}

	fd = open(filename, mode);
	if (fd < 0)
	{
		fprintf(stderr, "can't open file '%s' (mode: %d)\n", filename, mode);
		exit(3);
	}

	/* determine file size etc. */
	if (fstat(fd, &s) != 0)
	{
		fprintf(stderr, "can't stat file '%s'\n", filename);
		exit(4);
	}

	num_pages = (s.st_size / PAGE_SIZE);

	fprintf(stderr, "file size: %ld\n", s.st_size);
	fprintf(stderr, "file pages: %ld\n", num_pages);

	/* initialize the generator matching the workload type */
	if (strcmp(workload, "RANDOM") == 0)
	{
		if (argc != 8)
		{
			fprintf(stderr, "incorrect number of arguments (%d) for random workload\n", argc);
			exit(5);
		}

		next_page = random_next;
		random_init(&rstate, num_pages);
		gstate = (void *) &rstate;
	}
	else if (strcmp(workload, "SEQUENTIAL") == 0)
	{
		next_page = sequential_next;
		sequential_init(&sstate, num_pages, start_prob, continue_prob);
		gstate = (void *) &sstate;

		fprintf(stderr, "start probability: %.3f\n", start_prob);
		fprintf(stderr, "continue probability: %.3f\n", continue_prob);
	}
	else
	{
		fprintf(stderr, "unknown workload type\n");
		exit(6);
	}

	if (strcmp(test_mode, "WARMUP") == 0)
	{
		warmup = 1;
		retval = 1;
	}
	else if (strcmp(test_mode, "TEST") == 0)
	{
		warmup = 0;
		retval = 0;
	}
	else
	{
		fprintf(stderr, "unknown test mode\n");
		exit(7);
	}

	init_queue(&queue, queue_size, num_threads);

	/* create the I/O threads */
	threads = (pthread_t *) malloc(sizeof(pthread_t) * num_threads);
	for (i = 0; i < num_threads; i++)
	{
		if (pthread_create(&threads[i], NULL, prefetch_worker, &queue) != 0)
		{
			fprintf(stderr, "pthread_create failed\n");
			exit(7);
		}
	}

	gettimeofday(&tv1, NULL);
	elapsed = 0;
	next_elapsed = PROGRESS_INTERVAL;

	pthread_mutex_lock(&queue.lock);

	/* fill the I/O queue */
	while (1)
	{
		double	speed, remaining;

		if (queue.npending > 0)
		{
			fprintf(stderr, "unexpected state of the queue\n");
			exit(1);
		}

		for (i = 0; i < queue_size; i++)
		{
			queue.requests[i].fd = fd;
			queue.requests[i].offset = next_page(gstate) * PAGE_SIZE;
			queue.requests[i].nbytes = PAGE_SIZE;
		}

		/* say the queue is full */
		queue.next = 0;
		queue.nrequests = queue_size;
		queue.npending = queue_size;

		nrequests += queue_size;

		/* wake up the I/O threads */
		pthread_cond_broadcast(&queue.io_submitted);

		/* wait until all the requests get processed */
		while (queue.npending > 0)
			pthread_cond_wait(&queue.io_completed, &queue.lock);

		/* actually read the data, just like we'd do in BHS */
		if (strcmp(iomode, "BUFFERED") == 0)
			actually_read_data(&queue);

		/* print the progress info every 5 seconds or so */
		if (nrequests >= nrequests_progress)
		{
			double		speed;
			long int	nrequests_next;
			struct timeval	tv2;

			gettimeofday(&tv2, NULL);
			elapsed = (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec) / 1000000.0;

			/* requests per second */
			speed = (nrequests / elapsed);

			/* if we haven't reached the next progress threshold, just update
			 * the number of requests and continue */
			if (elapsed < next_elapsed)
			{
				nrequests_next = next_elapsed * speed;

				/*
				 * don't increment too fast - double at most
				 *
				 * The initial batches of requests may be unexpectedly high (due
				 * to caching or something like that), resulting in far too high
				 * threshold. Battle that by limiting the growth to doubling.
				 */
				if (nrequests_next > nrequests_progress * 2)
					nrequests_next = nrequests_progress * 2;

				nrequests_progress = nrequests_next;

				continue;
			}

			fprintf(stderr, "processed %ld requests %.2f r/s elapsed %.2f\n", nrequests, nrequests/elapsed, elapsed);

			/*
			 * compute the next point in time to print progress report, and
			 * the estimated number of records at that point
			 */
			next_elapsed = PROGRESS_INTERVAL * (floor(elapsed / PROGRESS_INTERVAL) + 1);
			nrequests_next = next_elapsed * speed;

			/*
			 * don't increment too fast - double at most
			 *
			 * The initial batches of requests may be unexpectedly high (due
			 * to caching or something like that), resulting in far too high
			 * threshold. Battle that by limiting the growth to doubling.
			 */
			if (nrequests_next > nrequests_progress * 2)
				nrequests_next = nrequests_progress * 2;

			nrequests_progress = nrequests_next;

			/* now decide what to do if we're in warmup mode */
			if (warmup)
			{
				/* first compute the speed in this interval (delta elapsed should be about PROGRESS_INTERVAL) */
				double		delta_elapsed = (elapsed - warmup_prev_elapsed);
				long int	delta_nrequests = (nrequests - warmup_prev_nrequests);
				double		delta_speed = delta_nrequests / delta_elapsed;

				fprintf(stderr, "elapsed %.2f : warmup interval speed %.2f previous %.2f\n", elapsed, delta_speed, warmup_prev_speed);

				/* if we got within 5% of the previous interval, warmup is done */
				if ((warmup_prev_nrequests > 0) && (delta_speed < warmup_prev_speed * 1.05))
				{
					fprintf(stderr, "within 5%% of the preceding interval, warmup terminating\n");
					retval = 0;
					break;
				}

				/* otherwise just remember data from this interval */
				warmup_prev_nrequests = nrequests;
				warmup_prev_speed = delta_speed;
				warmup_prev_elapsed = elapsed;
			}
			else if (elapsed >= max_duration)
			{
				/* test (non-warmup) mode ends when the time expires */
				break;
			}
		}
	}

	/* final message (print it always) */
	fprintf(stderr, "TOTAL: %ld requests %.2f r/s elapsed %.2f\n", nrequests, nrequests / elapsed, elapsed);

	pthread_mutex_unlock(&queue.lock);

	/* XXX we could/should close the files/threads here, but meh ... we're exiting anyway */

	return retval;
}
